<?php

function depurar($v) {
    echo '<pre>';
    var_dump($v);
    echo '</pre>';
}
$vocales=['a','e','i','o','u'];

$repeticiones=[
    'a'=>23,
    'e'=>1,
    'i'=>0,
    'o'=>40,
    'u'=>10
];

$repeticiones['o']++;
depurar($vocales);
depurar($repeticiones);
